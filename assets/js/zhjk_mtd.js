$(function(){
	$("#mtd_slfb_table tbody tr:odd").addClass("odd");
});
//全区上一日业务发生量折线图
var option = {
//  title: {
//      text: '全区上一日业务发生量'
//  },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    color:['#59c8e2','#a1e9d9'],
    legend: {
        data:['客流量','免填单业务']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
//  toolbox: {
//      feature: {
//          saveAsImage: {}
//      }
//  },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['08：00','10：00','12：00','14：00','16：00','18：00']
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name:'客流量',
            type:'line',
            stack: '总量',
            data:[120, 132, 101, 134, 90, 230, 210]
        },
        {
            name:'免填单业务',
            type:'line',
            stack: '总量',
            data:[220, 182, 191, 234, 290, 330, 310]
        }
    ]
};
var myChart = echarts.init(document.getElementById('mtd_fsl'));
myChart.setOption(option);


//全区免填单业务量占比饼图

option = {
//	title: {
//      text: '全区免填单业务量占比饼图'
//  },
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    color:['#88d7e7','#ffc501','#5ac8fb'],
    legend: {
        orient: 'vertical',
        data:['免填单占比','免单占比']
    },
    series: [
        {
            name:'全区业务量',
            type:'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            label: {
                normal: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    show: true,
                    textStyle: {
                        fontSize: '30',
                        fontWeight: 'bold'
                    }
                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data:[
                {value:100, name:'免填单占比'},
                {value:200, name:'免单占比'}
            ]
        }
    ]
};
var myChart = echarts.init(document.getElementById('mtd_ywl'));
myChart.setOption(option);





var map = new BMap.Map("mtd_bll");
map.centerAndZoom(new BMap.Point(111.73, 40.83), 6);
map.enableScrollWheelZoom();


var pointsData = [
	{
		name: "呼和浩特市",
		address: [111.73, 40.83]
	},
	{
		name: "包头市",
		address: [109.83, 40.65]
	},
	{
		name: "乌海市",
		address: [106.82, 39.67]
	},
	{
		name: "赤峰市",
		address: [118.92, 42.27]
	},
	{
		name: "通辽市",
		address: [122.27, 43.62]
	},
	{
		name: "鄂尔多斯市",
		address: [109.8, 39.62]
	},
	{
		name: "呼伦贝尔市",
		address: [119.77, 49.22]
	},
	{
		name: "巴彦淖尔市",
		address: [107.42, 40.75]
	},
	{
		name: "乌兰察布市",
		address: [113.12, 40.98]
	},
	{
		name: "兴安盟",
		address: [122.05, 46.08]
	},
	{
		name: "锡林郭勒盟",
		address: [116.07, 43.95]
	},
	{
		name: "阿拉善盟",
		address: [105.67, 38.83]
	}
];


var menu = new BMap.ContextMenu();
var txtMenuItem = [
	{
		text: "放大",
		callback: function() {
			map.zoomIn();
		}
	},
	{
		text: "缩小",
		callback: function() {
			map.zoomOut();
		}
	}
];
for (var i = 0; i < txtMenuItem.length; i++) {
	menu.addItem(
		new BMap.MenuItem(txtMenuItem[i].text, txtMenuItem[i].callback, 100)
	);
}
map.addContextMenu(menu);
function attribute(e) {
	var p = e.target;
	alert("当前的地理位置是" + p.getPosition().lng + "," + p.getPosition().lat);
}


getBoundary();
//
function getBoundary() {
	var bdary = new BMap.Boundary();
	bdary.get("内蒙古", function(rs) {
		//获取行政区域
		map.clearOverlays(); //清除地图覆盖物
		var count = rs.boundaries.length; //行政区域的点有多少个
		if (count === 0) {
			alert("未能获取当前输入行政区域");
			return;
		}
		var pointArray = [];
		for (var i = 0; i < count; i++) {
			var ply = new BMap.Polygon(rs.boundaries[i], {
				strokeWeight: 2,
				strokeColor: "#70A5DA",
				fillColor:"#DBE6F8"
			}); //建立多边形覆盖物
			map.addOverlay(ply); //添加覆盖物
			pointArray = pointArray.concat(ply.getPath());
		}
		var view = map.getViewport(eval(pointArray))
		var centerPoint = view.center;
		console.log(centerPoint)
		map.centerAndZoom(centerPoint, 5);
			//piont
		for (var i = 0; i < pointsData.length; i++) {
			map.addOverlay(new SquareOverlay("#BBDFF2", pointsData[i].address[0], pointsData[i].address[1],20));
			map.addOverlay(new SquareOverlay("#6BCDE4", pointsData[i].address[0], pointsData[i].address[1],10));
		}
	});
}

function SquareOverlay(color, x, y,len) {
	this._length = len;
	this._color = color;
	this._x = x;
	this._y = y;
  }
  // 继承API的BMap.Overlay
  SquareOverlay.prototype = new BMap.Overlay();
  // 实现初始化方法
  SquareOverlay.prototype.initialize = function(map) {
	// 保存map对象实例
	this._map = map;
	// 创建div元素，作为自定义覆盖物的容器
	var div = document.createElement("canvas");
	div.style.position = "absolute";
	// 可以根据参数设置元素外观
	div.style.width = this._length + "px";
	div.style.height = this._length + "px";
	div.style.background = this._color;
	div.style.borderRadius = this._length / 2 + "px";

	div.className = "btn-twinkle";
	div.onclick = function(e, a) {};
	// 将div添加到覆盖物容器中
	map.getPanes().markerPane.appendChild(div);
	// 保存div实例
	this._div = div;
	// 需要将div元素作为方法的返回值，当调用该覆盖物的show、
	// hide方法，或者对覆盖物进行移除时，API都将操作此元素。
	return div;
  };
  //实现绘制方法
  SquareOverlay.prototype.draw = function() {
	// 根据地理坐标转换为像素坐标，并设置给容器
	// var position = this._map.pointToOverlayPixel(this._center);
	var position = this._map.pointToOverlayPixel(
	  new BMap.Point(this._x, this._y)
	);
	this._div.style.left = position.x - this._length / 2 + "px";
	this._div.style.top = position.y - this._length / 2 + "px";
  };
  // 实现显示方法
  SquareOverlay.prototype.show = function() {
	if (this._div) {
	  this._div.style.display = "";
	}
  };
  // 实现隐藏方法
  SquareOverlay.prototype.hide = function() {
	if (this._div) {
	  this._div.style.display = "none";
	}
  };
  // 添加自定义方法
  SquareOverlay.prototype.toggle = function() {
	if (this._div) {
	  if (this._div.style.display == "") {
		this.hide();
	  } else {
		this.show();
	  }
	}
  };
