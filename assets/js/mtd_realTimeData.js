// $( "#starttime" ).datepicker({ timeFormat: 'hh:mm:ss'});

var myChart_bar = echarts.init(document.getElementById('mtd_realTimeData_con_mid_chartbar'));
var option_bar = {
	title: {
        text: '各盟市实时免填单业务办理数量',
        x: 'center',                 
    	y: 'top',                
    },
    tooltip : {
        trigger: 'axis',
        
       
    },
    color:["#56c7ea","#ffa10e"],
    legend: {
    	show: true ,
//  	backgroundColor:"#56c7ea",
        data:[{name:'昨日同期数',
        	   icon:'circle'},
        	  {name:'业务办理数',icon:'circle'}],
        x: 'left',              
    	y: '10px',
    	itemHeight  :9,
    	icon: "circle",
    	shadowColor: 'rgba(0, 0, 0, 0.5)',
    	shadowBlur: 10,
    	textStyle: {
	        color: '#b9c0ce' ,// 图例文字颜色
	    }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : ['呼和浩特','赤峰','包头','通辽','鄂尔多斯','巴彦淖尔','锡林浩特','呼伦贝尔','乌兰察布','兴安盟','阿拉善','乌海']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'昨日同期数',
            type:'bar',
            itemStyle: {
                normal: {
                    color: new echarts.graphic.LinearGradient(
                        0, 0, 0,1,
                        [
                            {offset:0, color: '#54c6ea'},
                            {offset: 0.5, color:'#8ed9f1'},
                            {offset: 1, color: '#c7ecf8'}
                        ]
                    )
                },
               
            },
            data:[320, 332, 301, 334, 390, 330, 320,310,360,290,309,280]
        },
        {
            name:'业务办理数',
            type:'bar',
            itemStyle: {
                normal: {
                    color: new echarts.graphic.LinearGradient(
                        0, 0, 0,1,
                        [
                            {offset:0, color: '#ff9c01'},
                            {offset: 0.5, color:'#ffb239'},
                            {offset: 1, color: '#ffd48f'}
                        ]
                    )
                }
            },
            data:[120, 132, 101, 134, 90, 230, 210,210,220,200,180,178]
        },
       
    ]
};
// 使用刚指定的配置项和数据显示图表。
 myChart_bar.setOption(option_bar);
 
var myChart_pie = echarts.init(document.getElementById('mtd_realTimeData_con_btm_leftpiechart'));
var option_pie = {
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    color:["#3687fb","#92bbdd","#bac0c3","#89c7c2","#85bfff","#a8e4df"],
    itemStyle: {
        normal: {
            // color: 各异,
            borderColor: '#fff',
            borderWidth: 4,
            label: {
                show: true,
                position: 'outer'
                // textStyle: null      // 默认使用全局文本样式，详见TEXTSTYLE
            },
            labelLine: {
                show: true,
                length: 30,
                lineStyle: {
//                  color: 各异,
                    width: 2,
                    type: 'dotted'
                }
            }
        },
         emphasis: {
            // color: 各异,
            borderColor: 'rgba(0,0,0,0)',
            borderWidth: 1,
            label: {
                show: false
            },
            labelLine: {
                show: true,
                length: 20,
                lineStyle: {
                    // color: 各异,
                    width: 1,
                    type: 'dotted'
                }
            }
        }
         
    },
    series : [
        {
            name: '实时免填单业务域占比情况',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:335, name:'法制域'},
                {value:310, name:'登记域'},
                {value:234, name:'发票域'},
                {value:135, name:'认定域'},
                {value:148, name:'征收域'},
                 {value:135, name:'优惠域'}
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
 myChart_pie.setOption(option_pie);