var myChart = echarts.init(document.getElementById('mtd_historicaldata_con_btm_chart'));
var option = {
    title: {
        text: '呼和浩特赛罕区服务中心历史数据',
        x: 'center',                 // 水平安放位置，默认为左对齐，可选为：
                               // 'center' ¦ 'left' ¦ 'right'
                               // ¦ {number}（x坐标，单位px）
    	y: 'top',                  // 垂直安放位置，默认为全图顶端，可选为：
                               // 'top' ¦ 'bottom' ¦ 'center'
                               // ¦ {number}（y坐标，单位px）
    },
    color:["#00ffba","#63b0e2","#f19149"],
    tooltip: {
        trigger: 'axis',
        backgroundColor: 'rgba(255,255,255,0.9)',
        textStyle: {
	        color: '#78859a'
	    },
        axisPointer: {
            type: 'cross'
        },
        formatter:"<img style='vertical-align: middle;display: inline-block;margin-right:5px;margin-top:8px;' src='../images/mtd_tooilp.png'/><span style='color:#717171;font-size:12px;'>{a0}：</span><br/><span style='margin-left:16px;color:#78859a;font-size:14px;'>{c0}</span><br /><img style='vertical-align: middle;display: inline-block;margin-right:5px;margin-top:8px;' src='../images/mtd_tooilp.png'/><span style='color:#717171;font-size:12px;'>{a1}：</span><br/><span style='margin-left:16px;color:#78859a;font-size:14px;'>{c1}</span><br /><img style='vertical-align: middle;display: inline-block;margin-right:5px;margin-top:8px;' src='../images/mtd_tooilp.png'/><span style='color:#717171;font-size:12px;'>{a2}：</span><br/><span style='margin-left:16px;color:#78859a;font-size:14px;'>{c2}</span>",
    },
    legend: {
        data:['发票域','法制域','登记域'],
        x: 'left',
    	y: '10px',
    	itemHeight  :9,
    	icon: "circle",
    	textStyle: {
	        color: '#b9c0ce'          // 图例文字颜色
	    }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },

    xAxis: {
        type: 'category',
        data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name:'发票域',
            type:'line',
            stack: '总量',
            data:[220, 132, 101, 134, 190, 230, 210,220, 132, 101, 134, 190]
        },
        {
            name:'法制域',
            type:'line',
            stack: '总量',
            data:[200, 112, 121, 114, 150, 220, 230,240, 122, 191, 124, 170]
        },
        {
            name:'登记域',
            type:'line',
            stack: '总量',
            data:[210, 122, 111, 124, 170, 200, 200,230, 112, 151, 154, 180]
        },

    ]
};


 // 使用刚指定的配置项和数据显示图表。
 myChart.setOption(option);