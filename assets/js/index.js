var option = {
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
		show:false,
        orient : 'vertical',
        x : 'right',
        data:['呼和浩特','锡林郭勒','赤峰','呼伦贝尔','阿拉善','包头','巴彦淖尔','通辽','兴安盟','乌兰察布','二连浩特','鄂尔多斯']
    },
    toolbox: {
        show : false,
        feature : {
            mark : {show: true},
            dataView : {show: true, readOnly: false},
            magicType : {
                show: true,
                type: ['pie', 'funnel'],
                option: {
                    funnel: {
                        x: '25%',
                        width: '50%',
                        funnelAlign: 'center',
                        max: 1548
                    }
                }
            },
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
	calculable : false,
	graphic:{
		type:'text',
		left:'center',
		top:'center',
		style:{
			text:'全区总量： 件 \n 6731',
			textAlign:'center',
			fill:'#807A75',
			width:40,
			height:40,
			textVerticalAlign:'middle',
			lineWidth:20,
			font: ' bolder 16px cursive'
		},

	},
    series : [
        {
            name:'访问来源',
            type:'pie',
            radius : ['60%', '70%'],
            itemStyle : {
                normal : {
                    label : {
                        show : false
                    },
                    labelLine : {
                        show : false
                    }
                },
                emphasis : {
                    label : {
                        show : true,
                        position : 'center',
                        textStyle : {
                            fontSize : '10',
                            fontWeight : 'bold'
                        }
                    }
                }
            },
            data:[
                {value:10, name:'呼和浩特'},
                {value:12, name:'锡林郭勒'},
                {value:8, name:'赤峰'},
                {value:7, name:'呼伦贝尔'},
                {value:9, name:'阿拉善'},
                {value:8, name:'包头市'},
                {value:10, name:'巴彦淖尔'},
                {value:12, name:'通辽市'},
                {value:10, name:'兴安盟'},
                {value:6, name:'乌兰察布'},
                {value:7, name:'二连浩特'},
                {value:9, name:'鄂尔多斯'},
            ]
        }
    ]
};
var myChart = echarts.init(document.getElementById('business_all'));
myChart.setOption(option);



