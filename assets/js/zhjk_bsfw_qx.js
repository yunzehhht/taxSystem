option = {
//  title : {
//      text: '某楼盘销售情况',
//      subtext: '纯属虚构'
//  },
    tooltip : {
        trigger: 'axis'
    },
    legend: {
    	x: 'left',
        data:['日变化量']
    },
//  toolbox: {
//      show : true,
//      feature : {
//          mark : {show: true},
//          dataView : {show: true, readOnly: false},
//          magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
//          restore : {show: true},
//          saveAsImage : {show: true}
//      }
//  },
	color:['#4cc3e0'],
    calculable : true,
    xAxis : [
        {
            type : 'category',
            boundaryGap : false,
            data : ['00：00','02：00','04：00','06：00','08：00','10：00','12：00','14：00','16：00','18：00','20：00','22：00']
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'成交',
            type:'line',
            smooth:true,
            itemStyle: {normal: {areaStyle: {type: 'default'}}},
            data:[0, 0, 0, 0, 0, 150, 200,450,0,0,600,0]
        }
    ]
};
var myChart = echarts.init(document.getElementById('bsfw_yw'));
myChart.setOption(option);
                    