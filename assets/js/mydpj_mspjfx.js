// var myChart_bar = echarts.init(document.getElementById('mydpj_qq_chartbar'));
var myChart = echarts.init(document.getElementById('mydpj_qq_chartbar'));

var optionBar = {
	title: {
        text: '赤峰市全区评价统计分析',
        x: 'center',
    	y: 'top'
    },
    tooltip : {
        trigger: 'axis',


    },
    color:["#66ffc7","#f9a214","#ff0000"],
    legend: {
    	show: true ,
        data:[{name:'服务人次',icon:'circle'},
        	  {name:'很满意',icon:'circle'},
        	  {name:'态度差',icon:'circle'},
        	  {name:'业务差',icon:'circle'}
        	  ],
        x: 'left',
    	y: '10px',
    	itemHeight  :9,
    	icon: "circle",
    	shadowColor: 'rgba(0, 0, 0, 0.5)',
    	shadowBlur: 10,
    	textStyle: {
	        color: '#b9c0ce' ,// 图例文字颜色
	    }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : ['红山区','松山区','元宝山区','宁城县 ','林西县','巴林左旗','巴林右旗','敖汉旗','翁牛特旗','哈喇沁旗','阿鲁科尔沁旗','科什克腾旗'],
        	axisLabel:{
		        interval:0,//横轴信息全部显示
		        rotate:-10,//-30度角倾斜显示
			}
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
			name:'服务人次',
            type:'bar',
            stack: '三',
            itemStyle: {
                normal: {
                    color: new echarts.graphic.LinearGradient(
                        0, 0, 0,1,
                        [
                            {offset:0, color: '#7facf5'},
                            {offset: 0.5, color:'#8ed9f1'},
                            {offset: 1, color: '#c7ecf8'}
                        ]
                    )
                },

            },
            data:[120, 232, 101, 234, 190,130, 220,120,260,290,109,180]
        },
		{
            name:'很满意',
            type:'bar',
            stack: '三',
            data:[220, 132, 101, 134, 190, 230, 210,220, 10, 101, 134, 190]
        },
        {
            name:'态度差',
            type:'bar',
            stack: '三',
            data:[200, 112, 121, 114, 150, 220, 230,240, 122, 191, 124, 170]
        },
        {
            name:'业务差',
            type:'bar',
            stack: '三',
            data:[210, 122, 111, 124, 170, 200, 200,230, 112, 151, 154, 180]
        },
    ]
};
// // 使用刚指定的配置项和数据显示图表。
myChart.setOption(optionBar);

var myChart_pie = echarts.init(document.getElementById('mydpj_qq_piechart'));
var yuan_color=[];
var data_name=[];
var data_val=[];
var sum_wz="共";
var option_pie = {
	title: {
        text:'评价总数:',
//      subtext:"45689",
        subtext:"1432",
        left:'center',
        top:'37%',
        // padding:[24,0],
        textStyle:{
          color:'#8c8e91',
          fontSize:18,
          align:'center',
          fontWeight:"200"
        },
        subtextStyle: {
	        color: '#757677',          // 副标题文字颜色
	        fontSize:30,
            align:'center',
            fontWeight:"600"
	    }
      },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        x: 'left',
        show: false,
        data:['无评价','满意','很满意','态度差','时间长','业务差','其他']
    },
    series : [
        {
            name: '全区各项评价占比情况',
            type: 'pie',
            radius: ['50%', '65%'],
            color:["#88dbf3","#2ba2d9","#4ccda7","#ff0000","#f378b5","#ffc501","#7cf1ff"],
            label: {
                normal: {show: false,
                    formatter: '{b} | {d}%  共：{c}',

                }
            },
            labelLine: {
                normal: {
                    show: false
                }
            },
            data:[
                {value:335, name:'无评价'},
                {value:310, name:'满意'},
                {value:234, name:'很满意'},
                {value:135, name:'态度差'},
                {value:148, name:'时间长'},
                {value:135, name:'业务差'},
                {value:135, name:'其他'},
            ],

        }
    ]
};
 myChart_pie.setOption(option_pie);