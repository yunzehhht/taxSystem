
//用户登录过期
function tokenCheck(){
	layer.alter("用户登录信息已过期，请重新登录!",{
		closeBtn:0
	},function(){
		location.href='/login.html'
	})
}
//设置cookie
function setCookie(name, value, day) {
	var date = new Date();
	date.setDate(date.getDate() + day);
	document.cookie = name + '=' + value + ';expires=' + date;
};
//获取cookie
function getCookie(name) {
	var reg = RegExp(name + '=([^;]+)');
	var arr = document.cookie.match(reg);
	if (arr) {
		if(name == 'USER_RYMC'){
			return decodeURI(arr[1]);
		}
		return arr[1];	
	} else {
		return '';
	}
};
//删除cookie
function delCookie(name) {
	setCookie(name, null, -1);
};
//弹框
function alert_block(message, surefun, num, cancelFun, id) {
	if (num == 1) {
		$('body').append(
			'<div class="tankOpacity">'
			+ '<div class="tank">'
			+ '<div class="tank-tit">'
			+ '<div class="tank-tit-tit">'
			+ '<div class="laba">'
			+ '<img src="img/show_ts.png" alt="">'
			+ '</div>'
			+ '<h5 class="tank-tit-tit-zi">提示:</h5>'
			+ '</div>'
			+ '<h5 class="bx" onclick="alert_close()">×</h5>'
			+ '</div>'
			+ '<div class="jg"><img src="img/jg.png" alt=""></div>'
			+ '<p class="tank-tit-tszi">' + message + '</p>'
			+ '<div class="tank-tj">'
			//				        +'<a>'
			+ '<button onclick="btnClick(' + surefun + ',' + id + ')" class="txd-list-sub1-sub" >确定</button>'
			//				        +'</a>'
			+ '<button  onclick="alert_close()" class="txd-list-sub1-sub">取消</button>'
			+ '</div>'
			+ '</div>'
			+ '</div>'
		);
	}
	if (num == 2) {
		$('body').append(
			'<div class="tankOpacity">'
			+ '<div class="tank">'
			+ '<div class="tank-tit">'
			+ '<div class="tank-tit-tit">'
			+ '<div class="laba">'
			+ '<img src="img/show_ts.png" alt="">'
			+ '</div>'
			+ '<h5 class="tank-tit-tit-zi">提示:</h5>'
			+ '</div>'
			+ '<h5 class="bx" onclick="alert_close(' + cancelFun + ')">×</h5>'
			+ '</div>'
			+ '<div class="jg"><img src="img/jg.png" alt=""></div>'
			+ '<p class="tank-tit-tszi">' + message + '</p>'
			+ '<div class="tank-tj">'
			+ '<div class="txd-list-sub2 num_one_btn">'
			+ '<button style="margin-left:0" onclick="alert_close()" class="txd-list-sub1-sub">确定</button>'
			+ '</div>'
			+ '</div>'
			+ '</div>'
			+ '</div>'
		);
	}
	if (num == 3) {
		$('body').append(
			'<div class="tankOpacity">'
			+ '<div class="tank">'
			+ '<div class="tank-tit">'
			+ '<div class="tank-tit-tit">'
			+ '<div class="laba">'
			+ '<img src="img/show_ts.png" alt="">'
			+ '</div>'
			+ '<h5 class="tank-tit-tit-zi">提示:</h5>'
			+ '</div>'
			+ '<h5 class="bx" onclick="alert_close(' + cancelFun + ')">×</h5>'
			+ '</div>'
			+ '<div class="jg"><img src="img/jg.png" alt=""></div>'
			+ '<p class="tank-tit-tszi">' + message + '</p>'
			+ '<div class="tank-tj">'
			+ '<div class="txd-list-sub2 num_one_btn">'
			+ '<button  onclick = "btnClick(' + surefun + ',' + id + ')" class="txd-list-sub1-sub">确定</button>'
			+ '</div>'
			+ '</div>'
			+ '</div>'
			+ '</div>'
		);
	}
}
function alert_close(callback) {
	typeof callback === "function" && callback()
	$(".tankOpacity").remove();
}
   
function close_list_showk(){
	$("#list_showk").remove();
}

function btnClick(callback, staff) {
	typeof callback === "function" && callback(staff)
}
var CheckObjectReg = function () { }
CheckObjectReg.prototype = {
	isPhone: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/);
		if (result == null) return false;
		return true;
	}
	,
	isMobile: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/^((\(\d{2,3}\))|(\d{3}\-))?((13\d{9})|(15\d{9})|(18\d{9})|(19\d{9})|(17\d{9}))$/);
		if (result == null) return false;
		return true;
	},
	//校验手机号
	isTel: function (text) {
		text = typeof text == "number" ? (text).toString() : text;
		if (this.isMobile(text) || this.isPhone(text)) return true;
		return false;
	},
	//匹配路径
	isUrl: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\’:+!]*([^<>\"])*$/);
		if (result == null) return false;
		return true;
	},
	//  匹配密码，以字母开头，长度在6-12之间，只能包含字符、数字和下划线。
	isPwd: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/^[a-zA-Z]\\w{6,12}$/);
		if (result == null) return false;
		return true;
	},
	// 判断是否包含中英文特殊字符，除英文"-_"字符外
	isContainsSpecialChar: function (str) {
		if (str == null || str == "") return false;
		var reg = RegExp(/[(\ )(\`)(\~)(\!)(\@)(\#)(\$)(\%)(\^)(\&)(\*)(\()(\))(\+)(\=)(\|)(\{)(\})(\')(\:)(\;)(\')(',)(\[)(\])(\.)(\<)(\>)(\/)(\?)(\~)(\！)(\@)(\#)(\￥)(\%)(\…)(\&)(\*)(\（)(\）)(\—)(\+)(\|)(\{)(\})(\【)(\】)(\‘)(\；)(\：)(\”)(\“)(\’)(\。)(\，)(\、)(\？)]+/);
		return reg.test(str);
	},
	//身份证号
	isId: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/);
		if (result == null) return false;
		return true;
	},
	//银行账户
	isBankCardId: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/([\d]{4})([\d]{4})([\d]{4})([\d]{4})([\d]{0,})?/);
		if (result == null) return false;
		return true;
	},
	//判断邮箱是否正确
	isMail:function(str){
		if (str == null || str == "") return false;
		　var reg =  RegExp(/^\w+((.\w+)|(-\w+))@[A-Za-z0-9]+((.|-)[A-Za-z0-9]+).[A-Za-z0-9]+$/);
		 return reg.test(str);
	},
	//商户号
	isShhID: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/^\d{18}$/);
		if (result == null) return false;
		return true;
	},
	isDmID: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{15,20}$/);
		if (result == null) return false;
		return true;
	},
	isXyID: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/^\d{15,20}$/);
		if (result == null) return false;
		return true;
	},
	//社会信用代码
	isXydmID: function (text) {
		text = typeof text == "number" ? (text).toString() : text;
		if (this.isXyID(text) || this.isDmID(text)) return true;
		return false;
	},
	//结算账户
	isJszhID: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/^\d{9}$/);
		if (result == null) return false;
		return true;
	},
	isJs_zhID: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/^\d{20}$/);
		if (result == null) return false;
		return true;
	},
	isJsZhID: function (text) {
		text = typeof text == "number" ? (text).toString() : text;
		if (this.isJs_zhID(text) || this.isJszhID(text) || this.isBankCardId(text)) return true;
		return false;
	},
	
	//封顶金额
	isFdID: function (str) {
		if (str == null || str == "") return false;
		var result = str.match(/^\d{1,3}$/);
		if (result == null) return false;
		return true;
	}
}
//阻止冒泡事件
function stopEvent(event) {
	var e = arguments.callee.caller.arguments[0] || event;
	if (e && e.stopPropagation) {
		e.stopPropagation();
	} else if (window.event) {
		window.event.cancelBubble = true;
	}
}