const gulp = require("gulp");
const del = require("del");
const autoPrefixer = require("gulp-autoprefixer");
const babel = require("gulp-babel");
const imagemin = require('gulp-imagemin');
const cleanCss = require("gulp-clean-css");
const uglify  = require('gulp-uglify')
const htmlmin = require('gulp-htmlmin')
gulp.task("clean", () => {
	del.sync("build");
});

//定义css流处理
gulp.task("css", () => {
	gulp.src("assets/css/*.css")
		.pipe(
			autoPrefixer({
				browsers: ["last 5 versions", "Firefox > 20", "IE >= 8"]
			})
		)
		.pipe(cleanCss({ compatibility: "ie8" }))
		.pipe(gulp.dest("build/css"));
});

//定义js流处理
gulp.task("js", function() {
	gulp.src(["assets/js/*.js"])
		.pipe(babel(
		))
		.pipe(uglify())
		.pipe(gulp.dest("build/js"));
	gulp.src('assets/plugins/**/*')
		.pipe(gulp.dest('build/plugins'))
});


//定义image流处理  主要做png图片处理
gulp.task("imgs", function() {
	gulp.src('assets/images/*')
		.pipe(imagemin())
		.pipe(gulp.dest('build/images'))

});

// //定义html流处理

gulp.task("html", function() {
	gulp.src('assets/**/*.html')
	.pipe( htmlmin({
		removeComments: true,               // 清除HTML注释
		collapseWhitespace: true,           // 压缩空格
		collapseBooleanAttributes: true,    // 省略布尔属性的值 <input checked="true"/> => <input checked>
		removeEmptyAttributes: true,        // 删除所有空格作属性值 <input id=""> => <input>
		removeScriptTypeAttributes: true,   // 删除<script>的type="text/javascript"
		removeStyleLinkTypeAttributes: true,// 删除<style>和<link>的type="text/css"
		minifyJS: true,                     // 压缩页面JS
		minifyCSS: true                     // 压缩页面CSS
	}))
	.pipe(gulp.dest('build'))
});



gulp.task("default", ["clean", "css","imgs","html", "js"], () => {
	console.log("done");
});
